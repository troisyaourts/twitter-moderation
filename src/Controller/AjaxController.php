<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Abraham\TwitterOAuth\TwitterOAuth;

class AjaxController
{
  public function indexAction(Application $app, Request $request) {
    $params = $request->query->all();
    
    $values = array('status' => 'ok');
    
    return $app->json($values);
  }

  public function sinceAction(Application $app, Request $request){
    //don't block session while long polling
    session_write_close();
    $since_id = $request->query->get('since_id');
    if(is_null($since_id)) return $app->json([]);


    $maxtime = ini_get('max_execution_time');
    $start = time();
    $twManager = $app['models']('Tweet');
    $qPending = $twManager->query()
                          ->where('status = "pending"')
                          ->andWhere('id > :since_id')
                          ->setParameter('since_id', $since_id)
                          ->orderBy('id', 'DESC')
                          ;
    

    while(1){

      $pending = $twManager->fetchAll($qPending);

      if(time() - $start > 0.9*$maxtime ) return $app->json([]);

      if($pending->count() > 0){

        $values = [];
        foreach ($pending as $t) {
          $values[] = ['ty_id' => $t->id,
                       'tweet_id' => $t->tweet_id,
                       'screen_name' => $t->screen_name, 
                       'text' => $t->text,
                       'images' => $t->getImages()];
        }

        return $app->json($values);

      }else{
        sleep(1);
        continue;
      }
    }
  }

  public function addtweetAction(Application $app, Request $request){
    $twManager = $app['models']('Tweet');
    $tweet_url = $request->request->get('tweet_url');

    $tweet_components = explode("/",$tweet_url);
    $tweet_id = end($tweet_components);

    if(empty($tweet_id)) return $app->json(["saved" => "no tweet found with this id or url"]);


    $connection = new TwitterOAuth($app['config']['twitter.api']['consumer_key'], 
                                   $app['config']['twitter.api']['consumer_secret'], 
                                   $app['config']['twitter.api']['access_token'], 
                                   $app['config']['twitter.api']['access_token_secret']);

    $status = $connection->get("statuses/show", array("id" => $tweet_id));

    if(!isset($status->id)) return $app->json(["saved" => "no tweet found with this id or url"]);

    $stripped_string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
 '|[\x00-\x7F][\x80-\xBF]+'.
 '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
 '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
 '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
 '--', $status->text );

    //reject overly long 3 byte sequences and UTF-16 surrogates and replace with --
    $stripped_string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.'|\xED[\xA0-\xBF][\x80-\xBF]/S','--', $stripped_string );
    $status->text = $stripped_string;

    $tweet = $app['models']("Tweet")->get();
    $tweet->data = $status;
    $tweet->tweet_id = $tweet->data['id'];
    $tweet->screen_name = $tweet->data['user']['screen_name'];
    $tweet->text = $tweet->data['text'];

    try{
      $tweet->save();
    }catch(\Exception $e){
      return $app->json(["saved" => "Tweet is a duplicate"]);
    }

    return $app->json(["saved" => "ok"]);

  }

  public function updatestatusAction(Application $app, Request $request, $id){
    $twManager = $app['models']('Tweet');
    $tweet = $twManager->get($id);

    if($tweet->isEmpty()) return $app->json(["saved" => "no tweet found with this id"]);

    $tweet->status = $request->request->get('status', 'pending');
    if($tweet->status == "canceled"){
      $tweet->delete();
    }else{
      $tweet->save();  
    }
    

    return $app->json(["saved" => "ok"]);

  }

  public function addtagAction(Application $app, Request $request, $id){
    $twManager = $app['models']('Tweet');
    $tweet = $twManager->get($id);
    if($tweet->isEmpty()) return $app->json(["saved" => "no tweet found with this id"]);

    $tag_id = $request->request->get('tag_id');

    $tagManager = $app['models']('Tag');
    $tag = $tagManager->get($tag_id);
    if($tag->isEmpty()) return $app->json(["saved" => "no tag found with this id"]);
    
    $tagged = $tweet->addTag($tag);

    if(!$tagged) return $app->json(["saved" =>"failed to save tag"]);

    return $app->json(["saved" => "ok", "tag" => $tag->toArray()]);
  }

  public function removetagAction(Application $app, Request $request, $id){
    $twManager = $app['models']('Tweet');
    $tweet = $twManager->get($id);
    if($tweet->isEmpty()) return $app->json(["saved" => "no tweet found with this id"]);

    $tag_id = $request->request->get('tag_id');

    $tagManager = $app['models']('Tag');
    $tag = $tagManager->get($tag_id);
    if($tag->isEmpty()) return $app->json(["saved" => "no tag found with this id"]);
    
    $tagged = $tweet->removeTag($tag);

    if(!$tagged) return $app->json(["saved" =>"failed to save tag"]);

    return $app->json(["saved" => "ok", "tag" => $tag->toArray()]);
  }
  
}

