<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AdminController
{
  public function indexAction(Application $app) {

    $twManager = $app['models']('Tweet');
    $qPending = $twManager
                    ->query()
                    ->where('status = "pending"')
                    ->orderBy('id', 'DESC')
                    ->setMaxResults(50);
                    ;
    $pending = $twManager->fetchAll($qPending);

    $qValid = $twManager
                    ->query()
                    ->where('status = "valid"')
                    ->orderBy('id', 'DESC')
                    //->setMaxResults(15);
                    ;
    $valid = $twManager->fetchAll($qValid);


    $qStarred = $twManager
                    ->query()
                    ->where('status = "starred"')
                    ->orderBy('id', 'DESC')
                    //->setMaxResults(15);
                    ;
    $starred = $twManager->fetchAll($qStarred);


    $tags = $app['models']('Tag')->fetchAll();

    $response = new Response($app['twig']->render('admin/index.html.twig', 
                                              compact("pending", 
                                                      "valid",
                                                      "starred",
                                                      "tags"
                                                      )
                                              ));
    return $response;
  }

  public function channelsAction(Application $app) {
    $channels = $app['models']('Tag')->fetchAll();
    $response = new Response($app['twig']->render('admin/channels.html.twig', 
                                              compact("channels")
                                              ));
    return $response;
  }

  public function addchannelAction(Application $app, Request $request) {
    $channel_data = $request->request->get('channel');
    $channel = $app['models']('Tag')->get();
    $channel->loadData($channel_data);
    $channel->save();
    return $app->redirect("/channels");
  }

  public function removechannelAction(Application $app, Request $request, $id) {
    $tag = $app['models']('Tag')->get($id);
    $tag->delete();

    return $app->redirect("/channels");
  }

  public function predicatesAction(Application $app) {
    $predicates = $app['models']('Predicate')->fetchAll();
    $response = new Response($app['twig']->render('admin/predicates.html.twig', 
                                              compact("predicates")
                                              ));
    return $response;
  }

  public function addpredicateAction(Application $app, Request $request) {
    $predicate_data = $request->request->get('predicate');
    $predicate = $app['models']('Predicate')->get();

    if($predicate_data['type'] == "location"){
      $predicate_data['predicate'] = json_decode($predicate_data['predicate']);
    }

    $predicate->loadData($predicate_data);
    $predicate->save();
    return $app->redirect("/predicates");
  }

  public function removepredicateAction(Application $app, Request $request, $id) {
    $predicate = $app['models']('Predicate')->get($id);
    $predicate->delete();

    return $app->redirect("/predicates");
  }

  public function activatepredicateAction(Application $app, Request $request, $id) {
    $predicate = $app['models']('Predicate')->get($id);
    $predicate->active = 1;
    $predicate->save();

    return $app->redirect("/predicates");
  }

  public function disablepredicateAction(Application $app, Request $request, $id) {
    $predicate = $app['models']('Predicate')->get($id);
    $predicate->active = 0;
    $predicate->save();

    return $app->redirect("/predicates");
  }

}

