<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Model\Tweet;
use Model\Tag;

class ApiController
{
  public function indexAction(Application $app, Request $request) {
    $params = $request->query->all();
    
    $values = array('status' => 'ok');
    
    return $app->json($values);
  }

  public function latestAction(Application $app, Request $request) {
    $params = $request->query->all();
    
    $twManager = $app['models']('Tweet');
    $q = $twManager->query()
                   ->setMaxResults(10)
                   ->orderBy('tweet_id', 'DESC')
                   ;

    $tweets = $twManager->fetchAll($q);
    $values = [];
    foreach ($tweets as $t) {
      $values[] = ['screen_name' => $t->screen_name, 
                   'text' => $t->text,
                   'images' => $t->getImages()
                   ];
    }
    
    return $app->json(["data" => $values]);
  }

  public function channelAction(Application $app, Request $request, $channel) {
    $params = $request->query->all();
    
    $twManager = $app['models']('Tweet');
    $q = $twManager->query()
                   ->setMaxResults(20)
                   ->join('o',Tweet::TABLE_LINK_TAG,'tt', 'tt.tweet_id = o.id')
                   ->join('tt', Tag::TABLE_NAME, 't', 't.id = tt.tag_id')
                   ->orderBy('tweet_id', 'DESC')
                   ->where('t.tag = :tag')
                   ->andWhere('status = "starred"')
                   ->setParameter('tag', $channel)
                   ;

    $tweets = $twManager->fetchAll($q);
    $values = [];
    foreach ($tweets as $t) {
      $values[] = ['screen_name' => $t->screen_name, 
                   'text' => $t->text,
                   'time' => date('H:i',strtotime($t->data['created_at'])),
                   'images' => $t->getImages()
                   ];
    }
    
    return $app->json(["data" => $values]);

  }
  
}

