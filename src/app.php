<?php
use Silex\Application;
use Silex\Provider;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use User\UserProvider;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/*
*  Init app and services providers
*/
$app            = new Silex\Application();
$app['debug']   = $config['debug'];
$app['config']  = $config;

$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());

// Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  // 'twig.options' => array('cache' => __DIR__.'/../cache', 'strict_variables' => true),
  'twig.options' => array('strict_variables' => true),
  'twig.path' => __DIR__.'/../views',
));

// Gestion des logs
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => __DIR__.'/../log/all.log',
  'monolog.name' => 'boostrap'
));

// Cache
$app->register(new Provider\HttpCacheServiceProvider(), array(
  'http_cache.cache_dir' => __DIR__.'/../cache/'
));

$app->register(new Provider\DoctrineServiceProvider(), array('db.options' => $config['db.options']));
$app->register(new Alpha\ServiceProvider());

// Login
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.encoder.digest'] = $app->share(function ($app) {
    // use the sha1 algorithm
    // don't base64 encode the password
    // use only 1 iteration
    return new MessageDigestPasswordEncoder('sha1', false, 1);
});
$app['security.firewalls'] = array(
  'login' => array(
    'pattern' => '^/login$',
  ),
  'api' => array(
    'pattern' => '^/api',
  ),
  'admin' => array(
    'pattern' => '^/',
    'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
    'logout' => array('logout_path' => '/logout'),
    'users' => array(
      'admin' => array('ROLE_ADMIN', '77b04f448f023cc3c4b84aba8ce2c9db8d98241e'),
    ),
    // 'users' => $app->share(function () use ($app) {
    //   return new UserProvider($app['db']);
    // }),
  ),
);

$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_EDITO'),
);
