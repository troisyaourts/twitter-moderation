<?php
use Symfony\Component\HttpFoundation\Request;

/*
 *  ADMIN
 */
$admin = $app['controllers_factory'];
$admin->match('/', 'Controller\AdminController::indexAction')->bind('home');
$admin->match('/login', function(Request $request) use ($app) {
  return $app['twig']->render('admin/login.html.twig', array(
    'error'         => $app['security.last_error']($request),
    'last_username' => $app['session']->get('_security.last_username'),
  ));
});
$admin->match('/channels', 'Controller\AdminController::channelsAction')->bind('channel_admin');
$admin->match('/predicates', 'Controller\AdminController::predicatesAction')->bind('predicate_admin');
$admin->post('/channel/add', 'Controller\AdminController::addchannelAction')->bind('channel_add');
$admin->post('/channel/{id}/delete', 'Controller\AdminController::removechannelAction')->bind('channel_remove');

$admin->post('/predicate/add', 'Controller\AdminController::addpredicateAction')->bind('predicate_add');
$admin->post('/predicate/{id}/delete', 'Controller\AdminController::removepredicateAction')->bind('predicate_remove');
$admin->post('/predicate/{id}/activate', 'Controller\AdminController::activatepredicateAction')->bind('predicate_activate');
$admin->post('/predicate/{id}/disable', 'Controller\AdminController::disablepredicateAction')->bind('predicate_disable');


$app->mount('/', $admin);

/*
 * AJAX
 */
$ajax = $app['controllers_factory'];
$ajax->match('/', 'Controller\AjaxController::indexAction')->bind('ajax');
$ajax->match('/since', 'Controller\AjaxController::sinceAction')->bind('since');
$ajax->post('/tweet/{id}/updatestatus', 'Controller\AjaxController::updatestatusAction')->bind('updatestatus');
$ajax->post('/tweet/{id}/add_tag', 'Controller\AjaxController::addtagAction')->bind('addtag');
$ajax->post('/tweet/{id}/remove_tag', 'Controller\AjaxController::removetagAction')->bind('removetag');
$ajax->post('/tweet/add', 'Controller\AjaxController::addtweetAction')->bind('addtweet');

$app->mount('/ajax', $ajax);


/*
 * API
 */
$api = $app['controllers_factory'];
$api->match('/', 'Controller\ApiController::indexAction')->bind('api');
$api->match('/latest', 'Controller\ApiController::latestAction')->bind('latest');
$api->get('/channel/{channel}', 'Controller\ApiController::channelAction')->bind('channel');
$app->mount('/api', $api);



$app->after(function (Request $req, $res) {
  if ($req->get('callback') !== null 
      && $req->getMethod() === 'GET')
  {
    $contentType = $res->headers->get(
      'Content-Type'
    );
    $jsonpContentTypes = array(
      'application/json',
      'application/json; charset=utf-8',
      'application/javascript',
    );
    if (!in_array($contentType, $jsonpContentTypes)) 
    {
      // Don't touch the response
      return;
    }
    if ($res instanceof JsonResponse) {
      $res->setCallBack($req->get('callback'));
    } else {
      $res->setContent(
        $req->get('callback') 
          . '(' . $res->getContent() . ');'
      );
    }
  }
});