<?php

namespace Model;

use Alpha\Entity;
use Doctrine\DBAL\Connection;

class Tweet extends Entity
{
  const TABLE_NAME = 'ty_tweet';
  const TABLE_LINK_TAG = 'ty_tweet_tag';


  public function getTags(){
    $tagManager = $this->manager->related('Tag');
    $q = $tagManager->query()
                    ->join('o', self::TABLE_LINK_TAG, 't', 'o.id = t.tag_id')
                    ->where("t.tweet_id = :tweet_id")
                    ->setParameter('tweet_id', $this->id)
                    ;
    return $tagManager->fetchAll($q);
  }

  public function addTag(Tag $tag){

    $sql = "INSERT IGNORE INTO ".self::TABLE_LINK_TAG
          ." SET  "
          ."  tweet_id = ? "
          .", tag_id = ? "
          ;

    $tagged = $this->manager->db->executeQuery($sql, array($this->id, $tag->id));
    if($tagged->errorCode() == "00000"){
      return true;
    }else{
      return false;
    }

  }
  public function removeTag(Tag $tag){

    $sql = "DELETE FROM ".self::TABLE_LINK_TAG
          ." WHERE  "
          ."  tweet_id = ? "
          ."AND tag_id = ? "
          ;

    $tagged = $this->manager->db->executeQuery($sql, array($this->id, $tag->id));
    if($tagged->errorCode() == "00000"){
      return true;
    }else{
      return false;
    }

  }

  public function getImages(){
    $images = [];

    if(isset($this->data["entities"])){
    if(isset($this->data["entities"]["media"])){
      foreach($this->data["entities"]["media"] as $m){
        if($m["type"] == "photo"){
          $images[] = $m["media_url"];
        }    
      }
    }}
    return $images;
  }
}