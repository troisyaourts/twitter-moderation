<?php

namespace Model;

use Alpha\Entity;
use Doctrine\DBAL\Connection;

class Predicate extends Entity
{
  const TABLE_NAME = 'ty_predicate';
}