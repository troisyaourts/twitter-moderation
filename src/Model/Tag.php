<?php

namespace Model;

use Alpha\Entity;
use Doctrine\DBAL\Connection;

class Tag extends Entity
{
  const TABLE_NAME = 'ty_tag';
  const TABLE_LINK_TWEET = 'ty_tweet_tag';
}