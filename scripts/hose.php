<?php
//check
$pid_file = __DIR__.'/../log/hose.pid';
$old_pid = (int)@file_get_contents($pid_file);

$running = ($old_pid!= 0) && (posix_kill($old_pid,0));
//it's running smoothly
if($running) {
  echo "Process already running with pid ".$old_pid."\n";
  exit;
}

//let's fork !
$pid = pcntl_fork();

if($pid == -1){
  echo "Could not fork :(\n";
} else if ($pid >= 1) {
  //parent : work here is done
  file_put_contents($pid_file, $pid);
  echo "Child launched with pid ".$pid."\n";
  exit;
}else{
  //chil
}

include(__DIR__.'/../scripts/bootstrap.php');
define("TWITTER_CONSUMER_KEY", $app['config']['twitter.api']['consumer_key']);
define("TWITTER_CONSUMER_SECRET", $app['config']['twitter.api']['consumer_secret']);

use Doctrine\DBAL\Exception\DriverException;

class TweetConsumer extends OauthPhirehose 
{

  const MYSQL_GONE_AWAY = 2006;
  private $app;

  protected $filterUpdMin = 10;

  public function __construct($username, $password,$app)
  {
    $this->app = &$app;
    return parent::__construct($username, $password, Phirehose::METHOD_FILTER);
  }
  public function enqueueStatus($raw){

    $status = json_decode($raw);

    if(isset($status->retweeted_status)){
      //do not save automatic RTs
      return;
    }

    if(substr($status->text, 0, 2) == "RT"){
      //do not save manual RTs
      return;
    }

    $stripped_string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
 '|[\x00-\x7F][\x80-\xBF]+'.
 '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
 '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
 '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
 '--', $status->text );

    //reject overly long 3 byte sequences and UTF-16 surrogates and replace with --
    $stripped_string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.'|\xED[\xA0-\xBF][\x80-\xBF]/S','--', $stripped_string );
    $status->text = $stripped_string;

    $tweet = $this->app['models']("Tweet")->get();
    $tweet->data = $status;
    $tweet->tweet_id = $tweet->data['id'];
    $tweet->screen_name = $tweet->data['user']['screen_name'];
    $tweet->text = $tweet->data['text'];

    $tweet->save();
    
    echo $tweet->screen_name."\t\t\t".$tweet->text."\n";

  }

  public function checkFilterPredicates()
  {
      $predicateManager = $this->app['models']('Predicate');
      $q = $predicateManager->query()->where('active = 1');
      $predicates = $predicateManager->fetchAll($q);
      $rules = ["track" => [], "location" => [], "follow" => []];
      foreach($predicates as $p){
        $rules[$p->filter][] = $p->predicate;
      }
      
      if(!empty($rules["track"])){
        $this->setTrack($rules["track"]);
      }else{
        $this->setTrack(null);
      }
      if(!empty($rules["location"])){
        $this->setLocations($rules["location"]);
      }else{
        $this->setLocations(null);
      }
      if(!empty($rules["location"])){
        $this->setFollow($rules["follow"]);
      }else{
        $this->setFollow(null);
      }
      
  }
}


$sc = new TweetConsumer($app['config']['twitter.api']['access_token'], $app['config']['twitter.api']['access_token_secret'], $app);
$sc->checkFilterPredicates();
$sc->setLang('fr');
$sc->consume();