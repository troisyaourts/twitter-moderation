<?php

$config = array();

$config['db.options'] = array(
	'dbname'  => 'PROJECT_DB',
	'host'   	=> 'localhost',
	'driver'  => 'pdo_mysql',
	'charset'	=> 'utf8',
    'driverOptions' => array(1002 => 'SET NAMES utf8'));
